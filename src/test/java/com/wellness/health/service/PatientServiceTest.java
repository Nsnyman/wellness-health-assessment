package com.wellness.health.service;

import com.wellness.health.entity.PatientEntity;
import com.wellness.health.translator.EntityModelTranslator;
import com.wellness.health.domain.PatientType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class PatientServiceTest {


    @Mock
    private EntityModelTranslator entityModelTranslator;

    @InjectMocks
    private PatientService patientService = new PatientServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void returnHighRiskIndividualDueToSystolicBloodPressureAndDiastolicBloodPressure() {
        PatientEntity entity = new PatientEntity("Test", "Test", "Test",
                1.8,80.0,141.0,91.0,30.0,"Test");

        PatientType patientType = new PatientType("Test", "Test", "Test",
                1.8,80.0,141.0,91.0,30.0, new Date(), "Test");

        Mockito.when(entityModelTranslator.mapTo(entity)).thenReturn(patientType);

        PatientType type = patientService.isHighRisk(patientType);

        assertThat(true, is(equalTo(type.getSystolicBloodPressure() == 141.0 && type.getDiastolicBloodPressure() == 91.0)));

    }

    @Test
    public void returnHighRiskIndividualDueToBMI() {
        PatientEntity entity = new PatientEntity("Test", "Test", "Test",
                1.8,80.0,139.0,91.0,31.0,"Test");

        PatientType patientType = new PatientType("Test", "Test", "Test",
                1.8,80.0,139.0,91.0,31.0, new Date(), "Test");

        Mockito.when(entityModelTranslator.mapTo(entity)).thenReturn(patientType);

        PatientType type = patientService.isHighRisk(patientType);

        assertThat(true, is(equalTo(type.getBmi() == 31.0)));

    }

    @Test
    public void returnNoHighRiskIndividual() {
        PatientEntity entity = new PatientEntity("Test", "Test", "Test",
                1.8,70.0,139.0,90.0,29.0,"Test");

        PatientType patientType = new PatientType("Test", "Test", "Test",
                1.8,70.0,139.0,90.0,29.0, new Date(),"Test");

        Mockito.when(entityModelTranslator.mapTo(entity)).thenReturn(patientType);

        PatientType type = patientService.isHighRisk(patientType);

        assertNull(type);

    }


}
