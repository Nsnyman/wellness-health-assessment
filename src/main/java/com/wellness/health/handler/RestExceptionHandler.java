package com.wellness.health.handler;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ResponseTypeHandler> foundPatientException(DataIntegrityViolationException ex) {
        ResponseTypeHandler error = new ResponseTypeHandler();
        error.setCode(HttpStatus.FOUND.value());
        error.setMessage("Duplicate Found");
        return new ResponseEntity<>(error, HttpStatus.FOUND);
    }


    @ExceptionHandler
    public ResponseEntity<ResponseTypeHandler> handleConstraintViolationException(MethodArgumentNotValidException e) {
        List errors = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
        String message = String.join(",", errors);
        if(message == null) {
            message = HttpStatus.BAD_REQUEST.getReasonPhrase();
        }
        ResponseTypeHandler error = new ResponseTypeHandler();
        error.setCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(message);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

}