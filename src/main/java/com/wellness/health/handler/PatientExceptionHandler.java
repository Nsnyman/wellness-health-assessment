package com.wellness.health.handler;

public class PatientExceptionHandler extends Exception {

    private static final long serialVersionUID = 1L;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public PatientExceptionHandler(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public PatientExceptionHandler() {
        super();
    }
}