package com.wellness.health.domain;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

public class CurrentUserType extends User {

	private UserType user;
	
	public CurrentUserType(UserType user) {
        super(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole()));
        this.user = user;
    }

    public UserType getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "CurrentUserType{" +
                "user=" + user +
                "} " + super.toString();
}
}
