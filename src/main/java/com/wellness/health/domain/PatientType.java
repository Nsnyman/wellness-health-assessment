package com.wellness.health.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

public class PatientType {

    @NotBlank(message = "{NotNull.patientType.required.name}")
    private String name;
    @NotBlank(message = "{NotNull.patientType.required.surname}")
    private String surname;
    @NotBlank(message = "{NotNull.patientType.required.email}")
    //Regex allowing email addresses permitted by RFC 5322
    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "{Invalid.patientType.email}")
    private String email;
    @NotNull(message = "{NotNull.patientType.required.height}")
    private Double height;
    @NotNull(message = "NotNull.patientType.required.weight")
    private Double weight;
    @NotNull(message = "NotNull.patientType.required.systolicBloodPressure")
    private Double systolicBloodPressure;
    @NotNull(message = "NotNull.patientType.required.diastolicBloodPressure")
    private Double diastolicBloodPressure;
    private Double bmi;
    private Date createdOn;
    private String createdBy;

    public PatientType() {
    }

    public PatientType(String name, String surname, String email, Double height, Double weight,
                       Double systolicBloodPressure, Double diastolicBloodPressure, Double bmi, Date createdOn, String createdBy) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.height = height;
        this.weight = weight;
        this.systolicBloodPressure = systolicBloodPressure;
        this.diastolicBloodPressure = diastolicBloodPressure;
        this.bmi = bmi;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(Double systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public Double getDiastolicBloodPressure() {
        return diastolicBloodPressure;
    }

    public void setDiastolicBloodPressure(Double diastolicBloodPressure) {
        this.diastolicBloodPressure = diastolicBloodPressure;
    }

    public Double getBmi() {
        return bmi;
    }

    public void setBmi(Double bmi) {
        this.bmi = bmi;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "PatientType{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", systolicBloodPressure=" + systolicBloodPressure +
                ", diastolicBloodPressure=" + diastolicBloodPressure +
                ", bmi=" + bmi +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
