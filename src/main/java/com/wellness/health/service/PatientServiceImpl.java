package com.wellness.health.service;

import com.wellness.health.entity.PatientEntity;
import com.wellness.health.translator.EntityModelTranslator;
import com.wellness.health.domain.PatientType;
import com.wellness.health.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    EntityModelTranslator entityModelTranslator;

    // logger
    private static final Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);

    // find all high risk patients
    @Override
    public List<PatientType> findAllHighRiskPatients() {
        // some logger message to illustrate logging at info level
        logger.info("finding high risk individuals");
        List<PatientEntity> entityList = patientRepository.findAll();
        List<PatientType> patientHighRiskList = new ArrayList<>();
        for (PatientEntity entity : entityList) {
            PatientType patientType = isHighRisk(entityModelTranslator.mapTo(entity));
            if (patientType != null) {
                patientHighRiskList.add(patientType);
            }
        }
        return patientHighRiskList;
    }

    //save patient
    @Override
    public PatientType save(PatientType patientType) {
        PatientEntity patientEntity = entityModelTranslator.mapTo(patientType);
        if (patientEntity != null) {
            PatientEntity  entity = patientRepository.save(patientEntity);
            return entityModelTranslator.mapTo(entity);
        }
        return null;
    }


    // check if patient is high risk
    @Override
    public PatientType isHighRisk(PatientType patientType) {
        if ((patientType.getBmi() > 30) || (patientType.getSystolicBloodPressure() > 140
                && patientType.getDiastolicBloodPressure() > 90)) {
            return patientType;
        }
        return null;
    }
}
