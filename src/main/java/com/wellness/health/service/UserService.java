package com.wellness.health.service;

import com.wellness.health.domain.UserType;

public interface UserService {
    UserType findByUsername(String username);
}
