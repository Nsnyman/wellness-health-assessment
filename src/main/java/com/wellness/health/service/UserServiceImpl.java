package com.wellness.health.service;

import com.wellness.health.entity.UserEntity;
import com.wellness.health.domain.UserType;
import com.wellness.health.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    public UserType findByUsername(String username) {
        UserEntity entity = userRepository.findByUsername(username);
        if(entity != null){
           return new UserType(entity.getUsername(),entity.getPassword(), entity.getRole());
        }
        return null;
    }

}
