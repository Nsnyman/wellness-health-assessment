package com.wellness.health.service;

import com.wellness.health.domain.PatientType;
import java.util.List;

public interface PatientService {

    // find all high risk patients
    List<PatientType> findAllHighRiskPatients();

    //save patient
    PatientType save(PatientType patientType);

    // check if patient is high risk
    PatientType isHighRisk(PatientType patientType);

}
