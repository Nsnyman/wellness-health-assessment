package com.wellness.health.controller;

import com.wellness.health.handler.ResponseTypeHandler;
import com.wellness.health.handler.PatientExceptionHandler;
import com.wellness.health.domain.PatientType;
import com.wellness.health.service.PatientService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class PatientController {

    @Autowired
    private PatientService patientService;


    @RequestMapping(value = "/patients/capture",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "capture patient result", response = ResponseTypeHandler.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully capture patient results.", response = ResponseTypeHandler.class),
            @ApiResponse(code = 500, message = "Internal server error.", response = ResponseTypeHandler.class)})
    public ResponseTypeHandler capturePatientDetails(@Valid @RequestBody PatientType patientType) throws PatientExceptionHandler {
        PatientType type= patientService.save(patientType);
        if(type == null) {
            return new ResponseTypeHandler(01, "Failed to captured patient results");
        }
        return new ResponseTypeHandler(02, "Successfully captured patient results");
    }

    @RequestMapping(value = "/patients/high-risk",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get high risk patients", response = PatientType.class, responseContainer = "List", authorizations = {
            @Authorization(value = "basicAuth",
                    scopes = @AuthorizationScope(scope = "ADMIN", description = "Admin based access"))})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved high risk patients.", response = PatientType.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Internal server error.", response = ResponseTypeHandler.class)})
    public List<PatientType> getHighRiskPatients() {
        return patientService.findAllHighRiskPatients();
    }

}
