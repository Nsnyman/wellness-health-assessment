package com.wellness.health.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppRestController {

	@RequestMapping("/user")
	public Principal validate(Principal principal) {
		return principal;
	}

}
