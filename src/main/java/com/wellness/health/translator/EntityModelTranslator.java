package com.wellness.health.translator;

import com.wellness.health.entity.PatientEntity;
import com.wellness.health.domain.PatientType;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;

@Component
public class EntityModelTranslator {

    private static DecimalFormat df2 = new DecimalFormat(".##");

    public PatientEntity mapTo(PatientType type) {
        PatientEntity entity = null;
        if (type != null) {

            Double bmi = 0.0;
            // calculate BMI to two decimal places
            if(type.getWeight() > 0 && type.getHeight() > 0) {
                 bmi = Double.valueOf(String.valueOf(
                        df2.format(type.getWeight() / type.getHeight() / type.getHeight())));
            }

            entity = new PatientEntity(
                    type.getName(),
                    type.getSurname(),
                    type.getEmail(),
                    type.getHeight(),
                    type.getWeight(),
                    type.getSystolicBloodPressure(),
                    type.getDiastolicBloodPressure(),
                    bmi,
                    type.getCreatedBy());
        }
        return entity;
    }

    public PatientType mapTo(PatientEntity entity) {
        PatientType patientType = null;
        if (entity != null) {
            patientType = new PatientType(
                    entity.getName(),
                    entity.getSurname(),
                    entity.getEmail(),
                    entity.getHeight(),
                    entity.getWeight(),
                    entity.getSystolicBloodPressure(),
                    entity.getDiastolicBloodPressure(),
                    entity.getBmi(),
                    entity.getCreatedOn(),
                    entity.getCreatedBy());
        }
        return patientType;
    }

}
