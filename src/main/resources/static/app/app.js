// Creating angular Application with module name "Wellness-health-app"
var app = angular.module('Wellness-health-app', []);

// If we implement the basic security in spring boot then the response will
// contains the header 'WWW-Authenticate: Basic'. So the browser will popup a
// alert to get the user credentials. To avoid that we have to set a header in
// every request we are making using AngularJs.
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);

// Creating the Angular Controller
app.controller('AppCtrl', function ($http, $scope) {

    $scope.patient = {};
    $scope.message = '';
    $scope.user = '';
    $scope.basicAuth = '';

    // method for login
    $scope.login = function () {
        // creating base64 encoded String from username and password
        var base64Credential = btoa($scope.username + ':' + $scope.password);

        // calling GET request for getting the user details
        $http.get('user', {
            headers: {
                // setting the Authorization Header
                'Authorization': 'Basic ' + base64Credential
            }
        }).success(function (res) {
            $scope.password = null;
            if (res.authenticated) {
                $scope.message = '';
                $scope.basicAuth = base64Credential;
                $scope.user = res;
            } else {
                $scope.message = 'Invalid username and password provided. Please try again !';
            }
        }).error(function (error) {
            console.log(error);
            $scope.message = 'Invalid username and password provided. Please try again !';
        });
    };


    // method for getting an Administrator Resource
    $scope.getHighRiskIndividuals = function () {
        $http.get('/v1/patients/high-risk',
            {
                withCredentials: true,
                headers: {
                    'Authorization': 'Basic ' + $scope.basicAuth
                }
            }).success(function (patients) {
            $scope.highRiskPatients = patients;
            $scope.highRisk = true;
        }).error(function (error) {
            $scope.highRisk = false;
            $scope.successMessage = "";
            $scope.errorMessage = error.message;
        });
    };


    // method for capturing a user
    $scope.capturePatient = function () {
        $scope.patient.createdBy = $scope.username;
        $http.post('/v1/patients/capture',
            JSON.stringify($scope.patient),
            {
                withCredentials: true,
                headers: {
                    'Authorization': 'Basic ' + $scope.basicAuth
                }

            })
            .success(function (res) {
                $scope.successMessage = "Patient captured successfully";
                $scope.errorMessage = "";
                $scope.patient = {};
                $scope.myForm.$setPristine();
            }).error(function (error) {
            $scope.successMessage = "";
            $scope.errorMessage = error.message;
        });
    };


    // method for logout
    $scope.logout = function () {
        // clearing the authorization header
        $http.defaults.headers.common['Authorization'] = null;
        // clearing all data
        $scope.user = null;
        $scope.resource = null;
        $scope.highRisk = false;
        $scope.capture = false;

    };
});