/*
 * HSQLDB script.
 * Load the database with reference data and unit test data.
 */
INSERT INTO user (username, password, role) VALUES ('admin', 'admin', 'ADMIN');
INSERT INTO user (username, password, role) VALUES ('user', 'user', 'CAPTURE');


INSERT INTO patient (name, surname, email, height, weight, systolic_blood_pressure, diastolic_blood_pressure, bmi, created_on, created_by) VALUES ('Nicholas', 'Snyman', 'nicholassnyman@outlook.com', 1.8, 80, 141, 92, 24.69, '2017-07-26', 'admin');