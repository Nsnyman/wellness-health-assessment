## Demonstrate Basic Coding Ability

**Please see Practical.pdf for all validations and additional requirements**

### High Level 

#### Login

The user is required to provide a username and password to access the system. Registration is out of scope for this phase. Two default user should be provided by the system for demonstration purposes.
The wellness application requires two user types. A admin user and a capturer user.

- Admin User: A admin user can Capture results and view high risk patients.
- Capturer User: A capturer user may only capture results.

1. The user enters their username and password.
2. The system should validate that the credentials provided is in the database.
    - When the user provided invalid credentials, the system should display the following error message: Invalid username and password provided. Please try again.
    - When the user provides valid credentials, the system should direct the user to the dashboard.

#### User Dashboard

**The dashboard should be the gateway to the other two functions of the system. As a user, you can capture results and view high risk patients provided you have the correct role.**
1. When a admin user is logged in both capture result and view results should display.
2. When a capture user is logged in only capture result should display.


#### View High Risk Patients
**As an admin user, I want to see all patients results captures that are high risk. A high-risk result can be determined by the following:**
- BMI > 30 - BMI can be calculate by (weight in kg / height in meters)/divided by height in meters.
- Systolic blood pressure > 140 and Diastolic blood pressure > 90.
The screen should display all results captured that meet the high-risk criteria in a table.

#### NOTE:

**There are two default users:**

- ADMIN
  - Username: admin
  - Password: admin

- CAPTURE
  - Username: user
  - Password: user

To access the swagger: {localhost}:{port}/swagger-ui.html 

ROOT: {localhost}:{port}/



